const mysql = require('mysql2');
const express = require('express');
const cors = require('cors');

const connection = mysql.createConnection({
    host: 'database',
    user: 'root',
    password: 'pass',
    port: '3306',
    database: 'my_database'
});

const app = express();

app.use(express.json());
app.use(cors());

var status = 'Connection failed. Try again later.\n';

connection.connect((error) => {
  if (error) {
    console.error('Failed to connect to MySQL due to:\n', error);
  }
  return;
});

status = 'Connected to MySQL!\n';

// Default connection checking endpoint:
app.get('/', (req, res) => res.send(status));

// Example GET data endpoint:
app.get('/data', (req, res) => {
  connection.query('SELECT * FROM data', (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result);
  });
});

// Example POST data endpoint:
app.post('/data', (req, res) => {
  const text = req.body.text;
  connection.query('INSERT INTO data SET ?', {text}, (error, results) => {
    if (error) {
      throw error
    }
    res.send('Success!\n');
  });
});

app.listen(5000, () => console.log("Proxy server is up & running!\n"));
