CREATE DATABASE IF NOT EXISTS my_database;
CREATE TABLE IF NOT EXISTS `my_database`.`data` (`text` VARCHAR(255));

INSERT INTO `my_database`.`data` (`text`) VALUES ('Hello from the database!');