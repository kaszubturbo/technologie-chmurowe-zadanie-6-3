#!/bin/bash

docker kill frontend proxy backend database
docker rm frontend proxy backend database
docker network rm frontend_network backend_network
