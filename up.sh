#!/bin/bash

docker network create frontend_network
docker network create backend_network

docker run \
	--name database \
	-v ./db:/docker-entrypoint-initdb.d \
	-e MYSQL_ROOT_PASSWORD=pass \
	--expose 3306 \
	--network backend_network \
	-d \
	mysql:latest
sleep 5   # Wait for the server to start.

docker run \
	--name backend \
	--expose 5000 \
	-dt \
	node:latest
docker network connect frontend_network backend
docker network connect backend_network backend
docker cp ./backend backend:/opt/app
docker exec -d backend sh -c 'cd /opt/app; yarn install; node index.js'
sleep 5   # Wait for the API server to start.

docker run \
	--name frontend \
	--network frontend_network \
	-p 3000:3000 \
	-dt \
	node:alpine
docker cp ./frontend.dev frontend:/opt/app
docker exec -d frontend sh -c 'cd /opt/app; npm start;'
sleep 5

docker run \
	--name proxy \
	-p 80:80 \
	--network frontend_network \
	-v ./proxy/default.conf:/etc/nginx/conf.d/default.conf \
	-d \
	nginx:latest
